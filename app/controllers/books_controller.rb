class BooksController < ApplicationController
  before_action :get_page
  before_action :get_a_book, only: [:edit, :update, :destroy]
  before_action :get_books, only: [:index, :edit]
  before_action :sign_in_before 

  def sign_in_before
    unless signed_in?
       redirect_to signin_path
     end 
  end

  def index
    @book = Book.new(released_date: Time.now)
  end

  def welcome

  end

  def login
    
  end

  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html {
          flash[:notice] = t('messages.success.create')
          redirect_to books_path
        }
        format.js
      else
        format.html { render action: 'index' }
        format.js { render 'message' }
      end
    end
  end

  def edit
    render action: 'index'
  end

  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html {
          flash[:notice] = t('messages.success.update')
          redirect_to books_path
        }
        format.js { render 'update', locals: { book: @book } }
      else
        format.html { render action: 'index' }
      end
    end
  end

  def destroy
    @book.destroy
    respond_to do |format|
      format.html {
        redirect_to books_path, notice: t('messages.success.delete')
      }
      format.js { render 'delete', locals: { book: @book } }
    end
  end

  def about
  end

private
    # Use callbacks to share common setup or constraints between actions.
    def get_page
      session[:page] ||= 1
      session[:page] = params[:page].to_i if params[:page]
    end
    def get_a_book
      @book = Book.find(params[:id])
    end
    def get_books_on_page(page=1)
      @books = Book.paginate(page: page, per_page: 10)
    end
    def get_books
      @books = get_books_on_page(session[:page])
      if session[:page] > 1 && @books.empty?
        session[:page] -= 1
        @books = get_books_on_page(session[:page])
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:title, :released_date, :author_id,
        :rating, :available)
    end
end
