class LikesController < ApplicationController
  def create
  	current_user.likes.create(book_id: params[:id])
  end

  def destroy
  	current_user.likes.destroy(book_id: params[:id])
  end
end
