class Book < ActiveRecord::Base
  validates :title, presence: true
  validates :author, presence: true
  belongs_to :author
  has_many :likes
  has_many :users ,through: :likes
end
