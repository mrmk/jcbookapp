class User < ActiveRecord::Base
	has_many :likes
	has_many :books ,through: :likes


	before_save { self.email = email.downcase }
	before_create :create_remember_token

	validates_length_of :username, :minimum => 4 ,:maximum => 20
	validates_length_of :name , :maximum => 100
	validates_format_of :password, :with => /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}/ ,
	:message => "password must contain at least one 6 character  one small letter one capital one number"
	validates :password, :username, :password_confirmation , presence: true            
	has_secure_password
	
	def User.new_remember_token
		SecureRandom.urlsafe_base64
	end

	def User.encrypt(token)
		Digest::SHA1.hexdigest(token.to_s)
	end

	private

	def create_remember_token
		self.remember_token = User.encrypt(User.new_remember_token)
	end
end
